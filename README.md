RKPMol Project
==============

To begin with this will be mostly a project to create
a basic DFT solver using various basis set types,
including but not limited to PU, RKP, gaussian, etc.,
basis sets. The bare bones will be built first. By
this is meant that we'll set up the basic atom class
(and molecule soon, for generalization to molecular
mechanics) and produce the matrices from the basis
set and atom centers.

Initial Steps
-------------

Start by creating the following in the given order:

* Atom class, which may be an extension of OBAtom
* Basis set class, which doesn't have to be very
  generalized yet
* Generate the inner product and stiffness matrices
* Linear solver for the eigenvalue problem, simple
  for now; use ARPACK

# vim: tw=55:ts=4:sts=4:sw=4:et:sta
