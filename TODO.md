THINGS TO DO
============

1. Extract Breathe into ext/breathe in this folder
2. Run sphinx-quickstart to make all the initial files
3. Change the name of the `source` folder within this
   to `doc` and update the corresponding configuration
   files to reflect this


