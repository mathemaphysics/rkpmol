#ifndef ATOM_HPP
#define ATOM_HPP

#include <iostream>
#include <string>
#include "rkpmol_config.hpp"

/**
 * Use RKPMol namespace for everything
 * internal to the RKPMol project
 */
namespace RKPMol
{
    /**
     * A class for representing atoms
     */
    class Atom
    {
        private:
            /**
             * The name of the atom, a
             * symbolic representation
             * intended to indicate the
             * the element
             */
            char name[RKPMOL_ATOM_NAME_LEN];
            char element[RKPMOL_ATOM_ELEMENT_LEN];
            int number;
            double mass;

        public:
            
    }

    /**
     * Class for representing sets of
     * atoms for use in molecule classes
     * and also independently
     */
    class Atoms
    {
        
    }
}

#endif /* !ATOM_HPP */

// vim: ts=4:sts=4:sw=4:et:sta
