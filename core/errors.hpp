#include <iostream>
#include <fstream>
#include "rkpmol_config.h"

/**
 * This namespace contains everything contributing to the
 * RKPMol project. If any extensions are built, they should
 * be placed inside `namespace RKPMol`.
 */
namespace RKPMol
{
    /**
     * Creating the base error object
     */
    class BaseError
    {
        public:
            BaseError( std::string msg = "Undefined", long code = 0x0 ); ///< This is a constructor
            ~BaseError(); ///< This is a destructor
            
            void printFull();
            bool codeSet();
            bool messageSet();
            int  setCode( long );
            int  setMessage( std::string );
            int  setMessageFormat( enum t_outfmt );

        private:
            long code;
            std::string message;
            enum t_outfmt format;
            
    };

    class ConfigError: public BaseError
    {
        public:
            ConfigError();
            ~ConfigError();

        private:
            int line;
            std::string param;

        protected:


    }
}

// vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab:smarttab
