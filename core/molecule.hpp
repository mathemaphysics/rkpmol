#ifndef MOLECULE_HPP
#define MOLECULE_HPP

#include "../rkpmol_config.h"
#include "objects.hpp"
#include "errors.hpp"

namespace RKPMol
{
    class Molecule: public BaseObject
    {
        /**
         * Initializes the Molecule object given only the
         * number of atoms, `natom`, in the constructor. This
         * is only *one* of the several versions of the
         * overloaded constructor for `Molecule`.
         *
         * \param natom The number of atoms intended to be
         *        part of the molecule initially; if not known
         *        then use another constructor
         * \return Returns reference to the new Molecule object
         */
        Molecule( int natom );          ///< Constructor with only number of atoms
        Molecule( std::string fname );  ///< Load the Molecule from a file
        ~Molecule();
    }
}

#endif /* MOLECULE_HPP */

// vim: tw=65:ts=4:sts=4:sw=4:et:sta
