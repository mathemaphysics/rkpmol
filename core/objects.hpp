#include <iostream>
#include <fstream>
#include "rkpmol_config.h"

/**
 * This namespace contains everything contributing to the
 * RKPMol project. If any extensions are built, they should
 * be placed inside `namespace RKPMol`.
 */
namespace RKPMol
{
    /**
     * This basic object class will contain the information and
     * methods which are required for everything that is used in
     * all child object types.
     */
    class BaseObject
    {
        public:
            BaseObject();       ///< Basic constructor for everything
            ~BaseObject();      ///< Basic destructor for everything

        private:
            std::string name;

    };

    /**
     * A basic geometric object. This contains everything
     * that you would expect a shape or volume or point would
     * have. This includes a dimension and a few other properties
     * discussed below.
     */
    class GeomObject: public BaseObject
    {
        public: 
            GeomObject();       ///< Constructor for a geometric object
            ~GeomObject();      ///< Destructor for a geometric object

        private:
            int dim;            ///< Default dimension of the object is 3

    };

    /**
     * A basic class for representing types of functions.
     * Intended for use inside of other objects for representing
     * multi-coordinate interactions in molecular forcefields.
     */
    class Function: public BaseObject
    {
        public:
            Function();         ///< Make a new function
            ~Function();        ///< Clean up your Function mess

        private:
            int dim;            ///< Input spatial dimension

    };

    /**
     * A potential is a collection of Functions which couple
     * various numbers of atoms and other coordinates to form
     * any number of interactions.
     */
    class Potential: public Function
    {
        public:
            Potential();        ///< Initialize a new Potential
            ~Potential();       ///< Clean up your Potential

        private:
            int maxnint;        ///< Maximum number of interacting atoms/coordinates

    };

    /**
     * A standard object representing the force field or potential energy
     * function with the most basic elements necessary for any type of force
     * field.
     */
    class Forcefield: public BaseObject
    {
        public:
            Forcefield();       ///< Build a new Forcefield
            ~Forcefield();      ///< Clean up an old Forcefield           

        private:
            int d;

    };

    /**
     * This is the object containing the most important element of the solver:
     * The basis. The basis determines what type of support the solver will be
     * working with. The mainstay in RKPMol is the reproducing kernel particle
     * basis described thoroughly in the Wiki site at 
     * [RKPMolWiki](https://bitbucket.org/mathemaphysics/rkpmol/wiki/Math).
     * The RKP basis has some specific properties which make it particularly
     * nice for solving PDEs in their weak formulation.
     */
    class Basis: public BaseObject
    {
        public:
            Basis();          ///< Partition of unity basis constructor
            ~Basis();         ///< Partition of unity basis destructor

        private:

        protected:

    };

    /**
     * This the namespace which is associated with the internal configuration
     * of the inner workings of RKPMol.
     */
    namespace Configuration
    {
        std::string name;
    }

}

// vim: tabstop=4:softtabstop=4:shiftwidth=4:expandtab:smarttab
