.. rkpmol documentation master file, created by
   sphinx-quickstart on Fri Aug 31 05:58:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rkpmol's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

Introduction
============

This code is intended to standardize the way things
are done within molecular dynamics codes for the future.
Before any of this moves forward, we should all probably
look into whether or not there exist any standards already
established by **ISO** or **NIST** for the molecular
dynamics variables common to all code.


Indices and tables
==================

.. doxygennamespace:: RKPMol
   :project: rkpmol
   :members:
   :outline:

.. vim: tw=55:ts=4:sts=4:sw=4:et:sta
