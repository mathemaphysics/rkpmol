#ifndef RKPMOL_CONFIG_H
#define RKPMOL_CONFIG_H

#define RKPMOL_NAME_LENGTH 64		///< Maximum name length of any object
#define RKPMOL_FILENAME_LENGTH 128	///< Maximum length of a file name

#define RKPMOL_ATOM_NAME_LEN 6      ///< Maximum atom name length for the atom type
#define RKPMOL_ATOM_ELEMENT_LEN 2   ///< Maximum atom element length

#endif /* !RKPMOL_CONFIG_H */

// vim: ts=4:sw=4:sts=4:et:sta
